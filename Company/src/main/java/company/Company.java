package company;

import employee.Answer;
import employee.IEmployee;

import java.math.BigDecimal;
import java.util.*;

/**
 * Author: Wojciech
 * Date: 01.12.13
 */
public class Company implements Iterable<IEmployee>{

    private List<IEmployee> employees;

    public Company() {
        employees = new ArrayList<>();
    }

    public void addEmployee(IEmployee employee) {
        employees.add(employee);
    }

    public Integer getNumberOfEmployees() {
        return employees.size();
    }

    public Map<IEmployee, String> getNamesOfEmployees() {
        Map<IEmployee, String> names = new HashMap<>();
        for (IEmployee employee: employees) {
            names.put(employee, employee.getName());
        }
        return names;
    }

    public Map<IEmployee, BigDecimal> getSalariesOfEmployees() {
        Map<IEmployee, BigDecimal> salaries = new HashMap<>();
        for (IEmployee employee : employees) {
            salaries.put(employee, employee.getSalary());
        }
        return salaries;
    }

    public List<IEmployee> getSatisfiedEmployees() {
        List<IEmployee> satisfiedEmployees = new ArrayList<>();
        for (IEmployee employee : employees) {
            if (employee.isSatisfied().equals(Answer.FINE)) {
                satisfiedEmployees.add(employee);
            }
        }
        return satisfiedEmployees;
    }

    @Override
    public Iterator<IEmployee> iterator() {
        return employees.iterator();
    }
}
