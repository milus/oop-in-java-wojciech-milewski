package company.iterators;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Stack;

/**
 * Created by Wojciech Milewski.
 */
public class DFSIterator<T> implements Iterator<T>{
    private final Stack<Iterator<T>> stack = new Stack<>();

    public DFSIterator(Iterator<T> iterator) {
        stack.add(iterator);
    }

    @Override
    public boolean hasNext() {
        setSuccessorOnTop();

        return (!stack.isEmpty());
    }

    private void setSuccessorOnTop() {
        while (!stack.isEmpty()) {
            if (!stack.peek().hasNext()) {
                stack.pop();
            }
        }
    }

    @Override
    public T next() {
        if (!hasNext()) {
            throw new NoSuchElementException("There is no next element");
        }

        final T next = stack.peek().next();
        if (next instanceof Iterable) {
            stack.push(((Iterable) next).iterator());
        }

        return next;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("This feature is not supported yet");
    }

    protected Stack<Iterator<T>> getStack() {
        return stack;
    }
}
