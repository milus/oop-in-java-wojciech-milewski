package company;

import employee.IEmployee;

/**
 * Created by Wojciech Milewski.
 */
public interface IPredicate {
    Boolean apply(IEmployee employee);
}
