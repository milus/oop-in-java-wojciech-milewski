package company.predicates;

import company.IPredicate;
import employee.IEmployee;

import java.math.BigDecimal;

/**
 * Created by Wojciech Milewski.
 */
public class SalaryOver9000Predicate implements IPredicate {
    private final static BigDecimal BOUNDARY = BigDecimal.valueOf(9000);

    @Override
    public Boolean apply(IEmployee employee) {
        return BOUNDARY.compareTo(employee.getSalary()) < 0;
    }
}
