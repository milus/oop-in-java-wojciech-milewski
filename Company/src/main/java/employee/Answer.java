package employee;

/**
 * Created by Wojciech Milewski on 12/7/13.
 */
public enum Answer {
    GOOD("My salary is good!"),
    FINE("My salary is fine!"),
    BAD("My salary is bad!");

    private String description;


    private Answer(String description) {
        this.description=description;
    }

    @Override
    public String toString() {
        return this.description;
    }

}
