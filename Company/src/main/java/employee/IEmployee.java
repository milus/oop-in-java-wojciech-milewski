package employee;

import java.math.BigDecimal;

public interface IEmployee {
    public String getName();
    public BigDecimal getSalary();
    public Answer isSatisfied();
    public String getRole();
    public String work();
    public void setSalary(BigDecimal salary);
    public void setCurrentTask(String currentTask);
}
