package employee.satisfactionStrategy;

import employee.Answer;
import employee.IEmployee;

import java.math.BigDecimal;

/**
 * Created by Wojciech Milewski on 12/8/13.
 */
public class SatisfactionStrategyBasedOnCountryAverage implements employee.ISatisfactionStrategy {
    private final BigDecimal COUNTRY_AVERAGE = BigDecimal.valueOf(2500);

    public SatisfactionStrategyBasedOnCountryAverage() {}

    public Answer getSatisfaction(IEmployee employee) {
        if (employee.getSalary().compareTo(COUNTRY_AVERAGE)<0) {
            return Answer.BAD;
        }
        return Answer.FINE;
    }
}
