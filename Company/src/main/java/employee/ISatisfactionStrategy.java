package employee;

/**
 * Created by Wojciech Milewski on 12/7/13.
 */
public interface ISatisfactionStrategy {
    public Answer getSatisfaction(IEmployee employee);
}
