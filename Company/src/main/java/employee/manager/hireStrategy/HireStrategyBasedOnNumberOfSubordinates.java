package employee.manager.hireStrategy;

import employee.IEmployee;
import employee.manager.IHireStrategy;
import employee.manager.Manager;

/**
 * Created by Wojciech Milewski on 12/8/13.
 */
public class HireStrategyBasedOnNumberOfSubordinates implements IHireStrategy {
    private final Integer LIMIT_OF_EMPLOYEES;

    public HireStrategyBasedOnNumberOfSubordinates(Integer limit_of_employees) {
        this.LIMIT_OF_EMPLOYEES = limit_of_employees;
    }

    @Override
    public Boolean getHirePossibility(Manager manager, IEmployee employee) {
        if (manager.getNumberOfSubordinates().compareTo(LIMIT_OF_EMPLOYEES)<0) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }
}
