package employee.manager;

import company.IPredicate;
import company.iterators.DFSIterator;
import company.iterators.PredicateIterator;
import employee.Answer;
import employee.IEmployee;
import employee.ISatisfactionStrategy;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Wojciech Milewski.
 */
public class Manager implements IManager {
    private final String name;
    private BigDecimal salary;
    private String role;
    private String currentTask;
    private ISatisfactionStrategy satisfactionStrategy;
    private IHireStrategy hireStrategy;
    private final List<IEmployee> subordinates = new ArrayList<>();

    public Manager(String name, BigDecimal salary, String role, String currentTask, ISatisfactionStrategy satisfactionStrategy, IHireStrategy hireStrategy) {
        this.name = name;
        this.salary = salary;
        this.role = role;
        this.currentTask = currentTask;
        this.satisfactionStrategy = satisfactionStrategy;
        this.hireStrategy = hireStrategy;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public Answer isSatisfied() {
        return satisfactionStrategy.getSatisfaction(this);
    }

    public String getRole() { return role;}

    public String work() { return currentTask; }

    @Override
    public void setSalary(BigDecimal salary) {
        this.salary=salary;
    }

    @Override
    public void setCurrentTask(String currentTask) {
        this.currentTask = currentTask;
    }

    public Boolean canHire(IEmployee employee) {
        return hireStrategy.getHirePossibility(this, employee);
    }

    public void hire(IEmployee employee) {
        if (!canHire(employee)) {
            throw new IllegalArgumentException("Can't hire given employee");
        }
        subordinates.add(employee);
    }

    public void fire(IEmployee employee) {
        if (!subordinates.contains(employee)) {
            throw new IllegalArgumentException("Given employee is not hired");
        }
        subordinates.remove(employee);
    }

    public Integer getNumberOfSubordinates() {
        return subordinates.size();
    }

    @Override
    public Iterator<IEmployee> iterator() {
        return new DFSIterator<>(subordinates.iterator());
    }

    @Override
    public Iterator<IEmployee> iterator(IPredicate predicate) {
        return new PredicateIterator(subordinates.iterator(), predicate);
    }
}
