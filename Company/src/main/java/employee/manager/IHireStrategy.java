package employee.manager;

import employee.IEmployee;

/**
 * Created by Wojciech Milewski on 12/7/13.
 */
public interface IHireStrategy {
    public Boolean getHirePossibility(Manager manager, IEmployee employee);
}
