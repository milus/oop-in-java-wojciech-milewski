package employee.manager;

import company.IPredicate;
import employee.IEmployee;

import java.util.Iterator;

/**
 * Created by Wojciech Milewski.
 */
public interface IManager extends IEmployee, Iterable<IEmployee> {
    public Boolean canHire(IEmployee employee);
    public void hire(IEmployee employee);
    public void fire(IEmployee employee);

    Iterator<IEmployee> iterator(IPredicate predicate);
}
