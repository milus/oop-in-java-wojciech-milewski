package employee.employee;

import employee.Answer;
import employee.IEmployee;
import employee.ISatisfactionStrategy;

import java.math.BigDecimal;

/**
 * Author: Wojciech
 * Date: 30.11.13
*/
public class Employee implements IEmployee {
    private final String name;
    private BigDecimal salary;
    private String role;
    private String currentTask;
    private ISatisfactionStrategy satisfactionStrategy;

    public Employee(String name, BigDecimal salary, String role, ISatisfactionStrategy satisfactionStrategy) {
        this.name = name;
        this.salary = salary;
        this.role = role;
        this.satisfactionStrategy = satisfactionStrategy;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public Answer isSatisfied() {
        return satisfactionStrategy.getSatisfaction(this);
    }

    public String getRole() { return role;}

    public String work() { return currentTask; }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    public void setCurrentTask(String currentTask) {
        this.currentTask = currentTask;
    }
}
