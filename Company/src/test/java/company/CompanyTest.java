package company;

import employee.Answer;
import employee.IEmployee;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

/**
 * Author: Wojciech
 * Date: 01.12.13
 */
@RunWith(MockitoJUnitRunner.class)
public class CompanyTest {
    private Company instance;

    @Mock
    IEmployee employee1;
    @Mock
    IEmployee employee2;
    @Mock
    IEmployee employee3;

    @Before
    public void setUp() throws Exception {
        instance = new Company();
    }

    @Test
    public void getNumberOfEmployees_ifNoEmployeesShouldGiveZero() {
        //given
        //when
        final Integer numberOfEmployees = instance.getNumberOfEmployees();
        //then
        assertThat(numberOfEmployees).isEqualTo(0);
    }

    @Test
    public void getNumberOfEmployees_ifHiredEmployeesShouldGiveNonZero() {
        //given
        instance.addEmployee(employee1);
        instance.addEmployee(employee2);
        instance.addEmployee(employee3);
        //when
        final Integer numberOfEmployees = instance.getNumberOfEmployees();
        //then
        assertThat(numberOfEmployees).isEqualTo(3);
    }

    @Test
    public void getNamesOfEmployees_ifNoEmployeesShouldGiveEmptyMap() {
        //given
        //when
        final Map<IEmployee, String> employeesNames= instance.getNamesOfEmployees();
        //then
        assertThat(employeesNames).isEmpty();
    }
    @Test
    public void getNamesOfEmployees_ShouldGiveListOfNames() {
        //given
        instance.addEmployee(employee1);
        instance.addEmployee(employee2);
        instance.addEmployee(employee3);
        given(employee1.getName()).willReturn("Zbyszek");
        given(employee2.getName()).willReturn("Bogdan");
        given(employee3.getName()).willReturn("Ryszard");
        Map<IEmployee, String> expected = new HashMap<>();
        expected.put(employee1, "Zbyszek");
        expected.put(employee2, "Bogdan");
        expected.put(employee3, "Ryszard");

        final Map<IEmployee, String> employeesNames = instance.getNamesOfEmployees();

        assertThat(employeesNames).isEqualTo(expected);
    }

    @Test
    public void getSalariesOfEmployees_ifNoEmployeesShouldGiveZero() {

        final Map<IEmployee, BigDecimal> salaries = instance.getSalariesOfEmployees();

        assertThat(salaries).isEmpty();
    }

    @Test
    public void getSalariesOfEmployees_ShouldGiveListOfSalaries() {
        //given
        instance.addEmployee(employee1);
        instance.addEmployee(employee2);
        instance.addEmployee(employee3);
        given(employee1.getSalary()).willReturn(BigDecimal.valueOf(1000));
        given(employee2.getSalary()).willReturn(BigDecimal.valueOf(2000));
        given(employee3.getSalary()).willReturn(BigDecimal.valueOf(1500));
        Map<IEmployee, BigDecimal> expected = new HashMap<>();
        expected.put(employee1, BigDecimal.valueOf(1000));
        expected.put(employee2, BigDecimal.valueOf(2000));
        expected.put(employee3, BigDecimal.valueOf(1500));
        //when
        final Map<IEmployee, BigDecimal> salaries = instance.getSalariesOfEmployees();
        //then
        assertThat(salaries).isEqualTo(expected);
    }

    @Test
    public void getSatisfiedEmployees_ifNoWorkersShouldGiveEmptyList() {

        final List<IEmployee> satisfiedEmployees = instance.getSatisfiedEmployees();

        assertThat(satisfiedEmployees).isEmpty();
    }

    @Test
    public void getSatisfiedEmployees_ShouldGiveListSatisfiedEmployees() {
        instance.addEmployee(employee1);
        instance.addEmployee(employee2);
        instance.addEmployee(employee3);
        given(employee1.getName()).willReturn("Zbyszek");
        given(employee2.getName()).willReturn("Bogdan");
        given(employee3.getName()).willReturn("Ryszard");
        given(employee1.isSatisfied()).willReturn(Answer.FINE);
        given(employee2.isSatisfied()).willReturn(Answer.BAD);
        given(employee3.isSatisfied()).willReturn(Answer.BAD);

        final List<IEmployee> satisfiedEmployees = instance.getSatisfiedEmployees();

        assertThat(satisfiedEmployees).containsExactly(employee1);
    }
}
