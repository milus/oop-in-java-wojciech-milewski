package company.predicates;

import company.IPredicate;
import employee.IEmployee;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

/**
 * Created by Wojciech Milewski.
 */
public class SalaryOver9000PredicateTest {
    private IPredicate predicate;

    @Before
    public void setUp() {
        predicate = new SalaryOver9000Predicate();
    }

    @Test
    public void apply_ShouldReturnFalseIfSalaryOfGivenEmployeeIsBelow9000() {
        //given
        IEmployee employee = mock(IEmployee.class);
        given(employee.getSalary()).willReturn(BigDecimal.valueOf(3000));
        //when
        final Boolean apply = predicate.apply(employee);
        //then
        assertThat(apply).isEqualTo(Boolean.FALSE);
    }

    @Test
    public void apply_ShouldReturnTrueIfSalaryOfGivenEmployeeIsOver9000() throws Exception {
        //given
        IEmployee employee = mock(IEmployee.class);
        given(employee.getSalary()).willReturn(BigDecimal.valueOf(10000));
        //when
        final Boolean apply = predicate.apply(employee);
        //then
        assertThat(apply).isEqualTo(Boolean.TRUE);
    }
}
