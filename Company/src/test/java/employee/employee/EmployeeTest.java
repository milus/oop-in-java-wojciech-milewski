package employee.employee;

import employee.IEmployee;
import employee.ISatisfactionStrategy;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.verify;


/**
 * Author: Wojciech
 * Date: 30.11.13
*/
@RunWith(MockitoJUnitRunner.class)
public class EmployeeTest {
    private final String NAME = "Jan Kowalski";
    private final BigDecimal SALARY = BigDecimal.valueOf(2000);
    private final String ROLE = "Secretary";
    private final String TASK = "My first task";
    private IEmployee instance;

    @Mock
    private ISatisfactionStrategy satisfactionStrategy;

    @Before
    public void setUp() throws Exception {
        instance = new Employee(NAME, SALARY, ROLE, satisfactionStrategy);
        instance.setCurrentTask(TASK);
    }

    @Test
    public void getName_shouldGiveCorrectName() throws Exception {
        //given

        //when
        final String name = instance.getName();
        //then
        assertThat(name).matches(NAME);

    }
    @Test
    public void getSalary_shouldGiveCorrectSalary() throws Exception {
        //given

        //when
        final BigDecimal salary = instance.getSalary();
        //then
        assertThat(salary).isEqualTo(SALARY);
    }

    @Test
    public void isSatisfied_checkIfUsedSatisfactionStrategy() throws Exception {
        //given
        //when
        instance.isSatisfied();
        //then
        verify(satisfactionStrategy).getSatisfaction(instance);
    }

    @Test
    public void getRole_shouldGiveCorrectRole() throws Exception {
        //given
        //when
        final String role = instance.getRole();
        //then
        assertThat(role).isEqualTo(ROLE);
    }

    @Test
    public void getJob_shouldGiveCorrectTask() throws Exception {
        //given
        //when
        final String task = instance.work();
        //then
        assertThat(task).isEqualTo(TASK);
    }

}
