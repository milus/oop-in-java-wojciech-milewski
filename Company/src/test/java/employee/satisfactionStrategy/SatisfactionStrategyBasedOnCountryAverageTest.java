package employee.satisfactionStrategy;

import employee.Answer;
import employee.IEmployee;
import employee.ISatisfactionStrategy;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

/**
 * Created by Wojciech Milewski on 12/7/13.
 */
public class SatisfactionStrategyBasedOnCountryAverageTest {
    private ISatisfactionStrategy instance;

    @Before
    public void setUp() throws Exception {
        instance = new SatisfactionStrategyBasedOnCountryAverage();
    }

    @Test
    public void getSatisfaction_shouldReturnLowWhenSalaryIsBelowAverage() {
        //given
        IEmployee employee = mock(IEmployee.class);
        given(employee.getSalary()).willReturn(BigDecimal.ONE);
        //when
        final Answer satisfaction = instance.getSatisfaction(employee);
        //then
        assertThat(satisfaction).isEqualTo(Answer.BAD);
    }

    @Test
    public void getSatisfaction_shouldReturnHighWhenSalaryIsEqualOrOverAverage() {
        //given
        IEmployee employee = mock(IEmployee.class);
        given(employee.getSalary()).willReturn(BigDecimal.valueOf(10000));
        //when
        final Answer satisfaction = instance.getSatisfaction(employee);
        //then
        assertThat(satisfaction).isEqualTo(Answer.FINE);
    }
}
