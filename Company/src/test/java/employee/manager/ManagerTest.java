package employee.manager;

import employee.IEmployee;
import employee.ISatisfactionStrategy;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;

import static com.googlecode.catchexception.CatchException.caughtException;
import static com.googlecode.catchexception.apis.CatchExceptionBdd.then;
import static com.googlecode.catchexception.apis.CatchExceptionBdd.when;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ManagerTest {
    private Manager instance;
    private final String NAME = "John Smith";
    private final BigDecimal SALARY = BigDecimal.valueOf(10000);
    private final String ROLE = "Secretary";
    private final String TASK = "My first task";

    @Mock
    ISatisfactionStrategy satisfactionStrategy;

    @Mock
    IHireStrategy hireStrategy;

    @Before
    public void setUp() throws Exception {
        instance = new Manager(NAME, SALARY, ROLE, TASK, satisfactionStrategy, hireStrategy);
    }

    @Test
    public void getName_shouldGiveCorrectName() throws Exception {
        //given

        //when
        final String name = instance.getName();
        //then
        assertThat(name).matches(NAME);

    }
    @Test
    public void getSalary_shouldGiveCorrectSalary() throws Exception {
        //given

        //when
        final BigDecimal salary = instance.getSalary();
        //then
        assertThat(salary).isEqualTo(SALARY);
    }

    @Test
    public void isSatisfied_checkIfUsedSatisfactionStrategy() throws Exception {
        //given

        //when
        instance.isSatisfied();
        //then
        verify(satisfactionStrategy).getSatisfaction(instance);
    }

    @Test
    public void getRole_shouldGiveCorrectRole() throws Exception {
        //given
        //when
        final String role = instance.getRole();
        //then
        assertThat(role).isEqualTo(ROLE);
    }

    @Test
    public void getJob_shouldGiveCorrectTask() throws Exception {
        //given
        //when
        final String task = instance.work();
        //then
        assertThat(task).isEqualTo(TASK);
    }

    @Test
    public void canHire_checkIfUsedHireStrategy() throws Exception {
        //given
        IEmployee employee = mock(IEmployee.class);
        //when
        instance.canHire(employee);
        //then
        verify(hireStrategy).getHirePossibility(instance, employee);
    }

    @Test
    public void hire_shouldThrowIllegalArgumentException() throws Exception {
        //given
        IEmployee employee = mock(IEmployee.class);
        given(instance.canHire(employee)).willReturn(Boolean.FALSE);
        //when
        when(instance).hire(employee);
        //then
        then(caughtException()).isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    public void hire_shouldReturnTrueIfCanHireEmployee() throws Exception {
        //given
        IEmployee employee = mock(IEmployee.class);
        given(instance.canHire(employee)).willReturn(Boolean.TRUE);
        //when
        instance.hire(employee);
        //then
        //TODO:then what?
    }

    @Test
    public void fire_shouldReturnFalseIfEmployeeIsNotEmployedByManager() throws Exception {
        //given
        IEmployee employee = mock(IEmployee.class);
        //when
        when(instance).fire(employee);
        //then
        then(caughtException()).isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    public void fire_shouldReturnTrueIfEmployeeIsEmployedByManager() throws Exception {
        //given
        IEmployee employee = mock(IEmployee.class);
        given(instance.canHire(employee)).willReturn(Boolean.TRUE);
        instance.hire(employee);        //assuming that hiring method is OK
        //when
        when(instance).fire(employee);
        //then
        //TODO:then what?
    }

}
