package employee.manager.hireStrategy;

import employee.IEmployee;
import employee.manager.IHireStrategy;
import employee.manager.Manager;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

/**
 * Created by Wojciech Milewski on 12/8/13.
 */
public class HireStrategyBasedOnNumberOfSubordinatesTest {
    private IHireStrategy instance;
    private final Integer LIMIT_OF_EMPLOYEES = 10;

    @Mock
    IEmployee employee;

    @Before
    public void setUp() throws Exception {
        instance = new HireStrategyBasedOnNumberOfSubordinates(LIMIT_OF_EMPLOYEES);
    }

    @Test
    public void getHirePossibility_shouldReturnTrueIfCurrentNumberOfEmployeesIsBelowThanLimit() throws Exception {
        //given
        Manager manager = mock(Manager.class);
        given(manager.getNumberOfSubordinates()).willReturn(0);
        //when
        final Boolean answer = instance.getHirePossibility(manager, employee);
        //then
        assertThat(answer).isEqualTo(Boolean.TRUE);
    }

    @Test
    public void getHirePossibility_shouldReturnFalseIfCurrentNumberOfEmployeesIsEqualOrAboveLimit() throws Exception {
        //given
        Manager manager = mock(Manager.class);
        given(manager.getNumberOfSubordinates()).willReturn(LIMIT_OF_EMPLOYEES);
        //when
        final Boolean answer = instance.getHirePossibility(manager, employee);
        //then
        assertThat(answer).isEqualTo(Boolean.FALSE);
    }


}
